/**
 * @file
 * Charts js file.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.viewsChart = {
    attach: function (context, settings) {
      var id = drupalSettings.chart.id;
      var short_title = drupalSettings.chart.short_title;
      var points = drupalSettings.chart.points;
      var data = drupalSettings.chart.data;
      var point_color = drupalSettings.chart.point_color;
      var line_color = drupalSettings.chart.line_color;

      console.log(drupalSettings.chart);

      var config = {
        type: 'line',
        data: {
          labels: points,
          datasets: [{
            label: short_title,
            backgroundColor: point_color,
            borderColor: line_color,
            data: data,
            fill: false,
          }]
        },
        options: {
          responsive: true,
          title: {
            display: true,
            text: short_title
          },
          tooltips: {
            mode: 'index',
            intersect: false,
          },
          hover: {
            mode: 'nearest',
            intersect: true
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Time'
              }
            }],
            yAxes: [{
              display: true,
              scaleLabel: {
                display: false,
                labelString: 'Value'
              }
            }]
          }
        }
      };

      var ctx = document.getElementById(id);
      var chart = new Chart(ctx, config);

    }
  };

})(jQuery, Drupal, drupalSettings);
