<?php

namespace Drupal\views_chart\Plugin\views\style;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render a list in charts.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_chart",
 *   title = @Translation("Views charts"),
 *   help = @Translation("Render a list in charts."),
 *   theme = "views_view_views_chart",
 *   display_types = { "normal" }
 * )
 */
class ViewsCharts extends StylePluginBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatter $date_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter')
    );
  }

  /**
   * Return TRUE if this style also uses fields.
   *
   * @return bool
   *   Return bool.
   */
  public function usesFields() {
    return TRUE;
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    if ($this->usesFields()) {
      $options = ['' => $this->t('- None -')];
      $field_labels = $this->displayHandler->getFieldLabels();
      $options += $field_labels;

      if (count($options) > 1) {
        $form['data_field'] = [
          '#type' => 'select',
          '#title' => $this->t('Data field'),
          '#options' => $options,
          '#required' => TRUE,
          '#default_value' => (isset($this->options['data_field'])) ? $this->options['data_field'] : '',
          '#description' => $this->t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
        ];

        $form['point_field'] = [
          '#type' => 'select',
          '#title' => $this->t('Point field'),
          '#options' => $options,
          '#required' => TRUE,
          '#default_value' => (isset($this->options['point_field'])) ? $this->options['point_field'] : '',
          '#description' => $this->t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
        ];
      }
    }

    // The short title.
    $form['short_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short title'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['short_title'])) ? $this->options['short_title'] : '',
      '#description' => $this->t('The short title.'),
    ];

    // The date/time format.
    /*   $form['time_format'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Time format'),
    '#required' => TRUE,
    '#default_value' => (isset($this->options['time_format'])) ?
    $this->options['time_format'] : 'H;i',
    '#description' => $this->t('The date time format.'),
    );*/

    // Extra CSS classes.
    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS classes'),
      '#default_value' => (isset($this->options['classes'])) ? $this->options['classes'] : 'view-amount',
      '#description' => $this->t('CSS classes for further customization of this display.'),
    ];

    // Extra CSS id.
    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Id Container for Chart'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['id'])) ? $this->options['id'] : 'view-amount-id',
      '#description' => $this->t('ID container for the chart.'),
    ];

    // Width chart block.
    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width Container'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['width'])) ? $this->options['width'] : '100%',
      '#description' => $this->t('Width container for the chart.'),
    ];

    // Height chart block.
    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height Container'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['height'])) ? $this->options['height'] : '600px',
      '#description' => $this->t('Height container for the chart.'),
    ];

    // Line color chart block.
    $form['line_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Line color'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['line_color'])) ? $this->options['line_color'] : '#006384',
      '#description' => $this->t('Line color for the chart.'),
    ];

    // Line color chart block.
    $form['point_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Point color'),
      '#required' => TRUE,
      '#default_value' => (isset($this->options['point_color'])) ? $this->options['point_color'] : '#FF6384',
      '#description' => $this->t('Point color for the chart.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $data = $points = [];

    foreach ($this->view->result as $row) {
      $entity = $row->_entity;
      $data[] = $this->getFieldValue($entity, $this->options['data_field']);
      $points[] = $this->getFieldValue($entity, $this->options['point_field']);
    }

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
    ];

    $build['#attached']['library'][] = 'views_chart/views_chart.chartjs';
    $build['#attached']['library'][] = 'views_chart/views_chart';

    $build['#attached']['drupalSettings']['chart'] = [
      'id' => $this->options['id'],
      'short_title' => $this->options['short_title'],
      'line_color' => $this->options['line_color'],
      'point_color' => $this->options['point_color'],
      'data' => $data,
      'points' => $points,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue($entity, $field_name) {
    $value = '';

    $item = $entity->get($field_name)->first();
    if (!empty($item) && !empty($item->getValue())) {
      $value = $item->getValue()['value'];
    }

    return $value;
  }

}
