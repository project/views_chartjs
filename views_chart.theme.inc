<?php

/**
 * @file
 * Theme for views_chart views.
 */

/**
 * {@inheritdoc}
 */
function template_preprocess_views_view_views_chart(&$variables) {
  $variables['attributes']['id'] = $variables['options']['id'];

  // $variables['attributes']['width'] = $variables['options']['width'];
  $variables['attributes']['height'] = $variables['options']['height'];
}
